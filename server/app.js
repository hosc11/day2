console.log("Starting...");
var express = require("express");

var app = express();
console.log(__dirname);

const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client"));

if (!NODE_PORT){
    console.log("Express Port is not set");
    process.exit();
}

app.use(function(req, res){
    console.log("app use");
    res.send("Sorry wrong door!");
});

app.listen(NODE_PORT,function() {
    console.log("Web App started at " + NODE_PORT);
})
